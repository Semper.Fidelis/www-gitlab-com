---
layout: markdown_page
title: "Transient bugs"
description: "Build tooling for transient issues and outline process for sustainable early mitigations"
canonical_path: "/company/team/structure/working-groups/transient-bugs/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | November 16, 2020 |
| Target End Date | January 31, 2021 |
| Slack           | [#wg_transient-bugs](https://gitlab.slack.com/archives/C01EUKUM5DK) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/14rB6o7udwgWitV9lB7S3fzjHBaaqrG_23WVz89mHqGo/edit#heading=h.gp5w1bjoz2ug) (only accessible from within the company) |
| Task Board      | [Issue board](https://gitlab.com/groups/gitlab-org/-/boards/2190215) |
| Bug Board       | [`bug::transient` board](https://gitlab.com/groups/gitlab-org/-/boards/2206756)

## Business Goal

Build tooling for transient issues and outline process to sustainable prevent transient issues early on. 

[Transient bugs](/handbook/engineering/quality/issue-triage/index.html#transient-bugs) have become our focus in FY21Q4 OKRs. We have multiple cross-functional efforts and KRs in both Development and Quality and want to shore-up the momentum on addressing these issues earlier with the appropriate tooling and process in place.

### Exit Criteria
1. Reword Engineering FY21Q4 OKRs to reflect proactive mitigation of transient issues. 
1. Build transient developer tooling to help reproduce transient bugs locally.
1. Build transient test pipeline to help detect transient bugs (GDK,CI,Test flaky reports).
1. Determine top 3 product groups where fixing transient bugs can help increase GMAU.
1. Update our documentation on architectural patterns that can prevent transient bugs.
1. Incorporate future transient bugs into existing triage process and prioritization.
1. Build a measurement to increase visibility of transient bugs.

### Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Christopher Lefelhocz | VP of Development              |
| Facilitator           | Mek Stittri           | Director of Quality            |
| Functional Lead       | Tim Zallmann          | Director of Development, Dev     |
| Functional Lead       | Ramya Authappan       | Quality Engineering Manager, Dev |
| Functional Lead       | Valerie Karnes        | Director of Product Design |
| Member                | Tanya Pazitny         | Quality Engineering Manager, Enablement |
| Member                | Darva Satcher         | Senior Engineering Manager, Create |
| Member                | Michelle Gill         | Engineering Manager, Create:Source Code |
| Member                | André Luís            | Engineering Manager, Create:Source Code,  Create:Code Review |
| Member                | Phil Hughes           | Staff Frontend Engineer, Create:Code Review |
| Member                | Mark Lapierre         | Senior Software Engineer in Test, Create:Source Code |
| Member                | Sofia Vistas          | Software Engineer in Test, Package:Package  |
| Member                | Erick Banks           | Senior Software Engineer in Test, Enablement:Search |
| Member                | Tiffany Rea           | Software Engineer in Test, Verify:Continuous Integration       |
